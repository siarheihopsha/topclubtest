package siarheihopsha.topclubtest;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import siarheihopsha.topclubtest.callback.OnIpSelectedListener;
import siarheihopsha.topclubtest.presenters.LoginPresenterImpl;
import siarheihopsha.topclubtest.utils.JsonFormatter;
import siarheihopsha.topclubtest.utils.SharedPreferences;
import siarheihopsha.topclubtest.views.IpDialog;
import siarheihopsha.topclubtest.views.LoginView;


public class MainActivity extends ActionBarActivity implements LoginView, View.OnClickListener, OnIpSelectedListener {

    private LoginPresenterImpl loginPresenter;
    private EditText IPEdit;
    private View IPSavedButton;
    private EditText login;
    private EditText password;
    private View loginBlock;
    private View button;
    private TextView errorText;
    private TextView buttonText;
    private View progressBar;
    private TextView tokenText;
    private TextView userInfoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        IPEdit = (EditText) findViewById(R.id.editIP);
        IPSavedButton = findViewById(R.id.buttonSavedIP);
        IPSavedButton.setOnClickListener(this);
        login = (EditText) findViewById(R.id.editLogin);
        password = (EditText) findViewById(R.id.editPassword);
        loginBlock = findViewById(R.id.loginContainer);
        button = findViewById(R.id.button);
        button.setOnClickListener(this);
        errorText = (TextView) findViewById(R.id.textError);
        buttonText = (TextView) findViewById(R.id.textButton);
        buttonText.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        tokenText = (TextView) findViewById(R.id.textToken);
        userInfoText = (TextView) findViewById(R.id.textUserInfo);
        SharedPreferences sharedPreferences = SharedPreferences.getInstance(MainActivity.this);
        sharedPreferences.addIP("http://test.topclub.by");
        if (sharedPreferences.existCredentials()) {
            login.setText(sharedPreferences.readLogin());
            password.setText(sharedPreferences.readPassword());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showError(String error) {
        errorText.setText(error);
        errorText.setVisibility(View.VISIBLE);
        button.setClickable(true);
        buttonText.setClickable(true);
    }

    @Override
    public void onGotToken(String token) {
        errorText.setVisibility(View.GONE);
        tokenText.setText(token);
        loginPresenter.loadUserInfo(token);
    }

    @Override
    public void showUserInfo(String user) {
        errorText.setVisibility(View.GONE);
        String text = new JsonFormatter().format(user);
        userInfoText.setText(text);
        button.setClickable(true);
        buttonText.setClickable(true);
    }

    @Override
    public Activity getContext() {
        return MainActivity.this;
    }

    @Override
    public void showProgress() {
        button.setBackgroundResource(R.drawable.button_loading_bg);
        buttonText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        button.setBackgroundResource(R.drawable.button_bg);
        buttonText.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
            case R.id.textButton:
                if (login != null && password != null && IPEdit != null) {
                    if (IPEdit.length() == 0) {
                        this.showError("Enter IP.");
                    }
                    if (login.length() == 0) {
                        this.showError("Enter login.");
                    } else if (password.length() == 0) {
                        this.showError("Enter password.");
                    } else {
                        button.setClickable(false);
                        buttonText.setClickable(false);
                        String IP = IPEdit.getText().toString();
                        if (!IP.contains("http://")) {
                            IP = "http://" + IP;
                        }
                        String loginText = login.getText().toString();
                        String passwordText = password.getText().toString();
                        loginPresenter = new LoginPresenterImpl(this, IP);
                        loginPresenter.validateCredentials(loginText, passwordText);
                    }
                }
                break;
            case R.id.buttonSavedIP:
                IpDialog dialog = new IpDialog();
                dialog.setArguments(this, MainActivity.this);
                dialog.show(getSupportFragmentManager(), "SAVEDIP");
                break;
        }
    }

    @Override
    public void onIpSelected(String ip) {
        IPEdit.setText(ip);
    }
}
