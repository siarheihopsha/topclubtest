package siarheihopsha.topclubtest.presenters;

import android.content.Context;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public interface LoginPresenter {
    public void validateCredentials(String userName, String password);
    public void loadUserInfo(String token);
}
