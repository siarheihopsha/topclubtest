package siarheihopsha.topclubtest.presenters;

import android.content.Context;

import siarheihopsha.topclubtest.callback.OnBaseFinishedListener;
import siarheihopsha.topclubtest.callback.OnLoginFinishedListener;
import siarheihopsha.topclubtest.core.interactor.LoginInteractor;
import siarheihopsha.topclubtest.core.interactor.LoginInteractorImpl;
import siarheihopsha.topclubtest.entity.Token;
import siarheihopsha.topclubtest.utils.SharedPreferences;
import siarheihopsha.topclubtest.views.LoginView;

import retrofit.RetrofitError;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public class LoginPresenterImpl implements LoginPresenter{

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    private OnLoginFinishedListener onLoginFinishedListener = new OnLoginFinishedListener() {

        @Override
        public void onError(RetrofitError error) {
            loginView.hideProgress();
            loginView.showError(error.getMessage());
        }

        @Override
        public void onSuccess(Token token, String login, String password, String url) {
            SharedPreferences sharedPreferences = SharedPreferences.getInstance(loginView.getContext());
            sharedPreferences.writeLogin(login);
            sharedPreferences.writePassword(password);
            sharedPreferences.addIP(url);
            loginView.hideProgress();
            loginView.onGotToken(token.getToken());
        }
    };

    private OnBaseFinishedListener onBaseFinishedListener = new OnBaseFinishedListener() {
        @Override
        public void error() {
            loginView.hideProgress();
            loginView.showError("Error while loading user info.");
        }

        @Override
        public void onSuccess(String user) {
            loginView.hideProgress();
            loginView.showUserInfo(user);
        }
    };

    public LoginPresenterImpl(LoginView loginView, String url) {
        this.loginView = loginView;
        loginInteractor = new LoginInteractorImpl(url);
    }

    @Override
    public void validateCredentials(String userName, String password) {
        loginView.showProgress();
        loginInteractor.login(userName, password, onLoginFinishedListener);
    }

    @Override
    public void loadUserInfo(String token) {
        loginView.showProgress();
        loginInteractor.loadUserInfo("Token " + token, onBaseFinishedListener);
    }
}
