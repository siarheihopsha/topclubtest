package siarheihopsha.topclubtest.callback;

/**
 * Created by maksimsurpo on 13.05.15.
 */
public interface OnBaseFinishedListener {
    public void error();

    public void onSuccess(String user);
}
