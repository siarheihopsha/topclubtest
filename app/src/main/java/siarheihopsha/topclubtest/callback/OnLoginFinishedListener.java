package siarheihopsha.topclubtest.callback;

import siarheihopsha.topclubtest.entity.Token;

import retrofit.RetrofitError;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public interface OnLoginFinishedListener {

    public void onError(RetrofitError error);

    public void onSuccess(Token token, String login, String password, String url);

}
