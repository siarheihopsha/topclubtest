package siarheihopsha.topclubtest.callback;

/**
 * Created by Siarhei Hopsha on 27.07.2015.
 */
public interface OnIpSelectedListener {
    public void onIpSelected(String ip);
}
