package siarheihopsha.topclubtest.core.services;

import siarheihopsha.topclubtest.entity.Token;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public interface AuthorizationService {

    @FormUrlEncoded
    @POST("/api-token-auth/")
    public void login(@Field("username") String userName, @Field("password") String password, Callback<Token> callback);

    @GET("/api/get_user_info/")
    public void loadUserInfo(@Header("Authorization") String authorization, Callback<Response> callback);

}
