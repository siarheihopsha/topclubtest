package siarheihopsha.topclubtest.core.interactor;

import siarheihopsha.topclubtest.callback.OnBaseFinishedListener;
import siarheihopsha.topclubtest.callback.OnLoginFinishedListener;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public interface LoginInteractor {

    void login(String userName, String password, OnLoginFinishedListener onLoginFinishedListener);

    void loadUserInfo(String authorization, OnBaseFinishedListener onBaseFinishedListener);
}
