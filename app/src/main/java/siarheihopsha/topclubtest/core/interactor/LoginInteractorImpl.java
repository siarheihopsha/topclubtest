package siarheihopsha.topclubtest.core.interactor;

import android.util.Log;

import siarheihopsha.topclubtest.BuildConfig;
import siarheihopsha.topclubtest.callback.OnBaseFinishedListener;
import siarheihopsha.topclubtest.callback.OnLoginFinishedListener;
import siarheihopsha.topclubtest.core.services.AuthorizationService;
import siarheihopsha.topclubtest.entity.Token;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginInteractorImpl implements LoginInteractor {

    private String url;

    public LoginInteractorImpl(String url) {
        this.url = url;
    }

    @Override
    public void login(final String userName, final String password, final OnLoginFinishedListener onLoginFinishedListener) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        AuthorizationService authorizationService = restAdapter.create(AuthorizationService.class);
        authorizationService.login(userName, password, new Callback<Token>() {
            @Override
            public void success(Token o, Response response) {
                Log.d("TAG", o.getToken() + " RESP = " + response.toString());
                onLoginFinishedListener.onSuccess(o, userName, password, url);
            }

            @Override
            public void failure(RetrofitError error) {
                if (error != null) {
                    onLoginFinishedListener.onError(error);
                }
            }
        });
    }

    @Override
    public void loadUserInfo(String authorization, final OnBaseFinishedListener onBaseFinishedListener) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        AuthorizationService authorizationService = restAdapter.create(AuthorizationService.class);
        authorizationService.loadUserInfo(authorization, new Callback<Response>() {
            @Override
            public void success(Response userResponse, Response response) {
                Log.d("TAG", userResponse.toString() + " RESP = " + response.toString());
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(userResponse.getBody().in()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String result = sb.toString();
                onBaseFinishedListener.onSuccess(result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                onBaseFinishedListener.error();
            }
        });

    }
}
