package siarheihopsha.topclubtest.entity;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
