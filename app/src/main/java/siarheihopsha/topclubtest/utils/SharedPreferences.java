package siarheihopsha.topclubtest.utils;

import android.app.Activity;
import android.content.Context;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Siarhei Hopsha on 26.05.2015.
 */
public class SharedPreferences {

    private Context context;
    private static SharedPreferences instance;

    private SharedPreferences(Context context) {
        this.context = context;
    }

    public static SharedPreferences getInstance(Context context) {
        instance = new SharedPreferences(context);
        return instance;
    }

    public void writeLogin(String login) {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SHARED_PREFERENCES_LOGIN, login);
        editor.apply();
    }

    public String readLogin() {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.SHARED_PREFERENCES_LOGIN, "");
    }

    public void writePassword(String password) {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SHARED_PREFERENCES_PASSWORD, password);
        editor.apply();
    }

    public String readPassword() {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.SHARED_PREFERENCES_PASSWORD, "");
    }

    public boolean existCredentials() {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        return sharedPref.contains(Constants.SHARED_PREFERENCES_LOGIN);
    }

    public void addIP(String ip) {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        Set<String> set = sharedPref.getStringSet(Constants.SHARED_PREFERENCES_IP, new HashSet<String>());
        if (!set.contains(ip)) {
            set.add(ip);
            android.content.SharedPreferences.Editor editor = sharedPref.edit();
            editor.putStringSet(Constants.SHARED_PREFERENCES_IP, set);
            editor.apply();
        }
    }

    public CharSequence[] getIPList() {
        android.content.SharedPreferences sharedPref = ((Activity) context).getPreferences(Context.MODE_PRIVATE);
        Set<String> set = sharedPref.getStringSet(Constants.SHARED_PREFERENCES_IP, new HashSet<String>());
        return set.toArray(new CharSequence[set.size()]);
    }

}
