package siarheihopsha.topclubtest.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Siarhei Hopsha on 27.07.2015.
 */
public class JsonFormatter {
    public String format(String input) {
        JSONObject object = null;
        try {
            object = new JSONObject(input);
            return object.toString(3);
        } catch (JSONException e) {
            return input;
        }
    }
}
