package siarheihopsha.topclubtest.views;


import android.app.Activity;

/**
 * Created by maksimsurpo on 24.04.15.
 */
public interface LoginView {

    public void showProgress();
    public void hideProgress();
    public void showError(String error);
    public void onGotToken(String token);
    public void showUserInfo(String user);
    public Activity getContext();
}
