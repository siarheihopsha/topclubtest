package siarheihopsha.topclubtest.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import siarheihopsha.topclubtest.R;
import siarheihopsha.topclubtest.callback.OnIpSelectedListener;
import siarheihopsha.topclubtest.utils.SharedPreferences;

/**
 * Created by Siarhei Hopsha on 27.07.2015.
 */
public class IpDialog extends DialogFragment {

    private OnIpSelectedListener listener;
    private CharSequence[] items;

    public void setArguments(OnIpSelectedListener listener, Activity context) {
        this.listener = listener;
        this.items = SharedPreferences.getInstance(context).getIPList();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select IP")
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onIpSelected(items[which].toString());
                    }
                });
        return builder.create();
    }
}
